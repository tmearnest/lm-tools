#!/usr/bin/python3

# tme: determine face normal before transforming into triangle
# come up with a face mesh to apply to each quad



import argparse, itertools, h5py, yaml, re, logging, numpy, random, json

class FrameSlice:
    @property
    def slice(self):
        return slice(self.s[0],self.s[1],self.s[2])

    def __init__(self, s):
        s = str(s)
        ss = s.split(':')
        ss += ['', '', '']
        ss = ss[:3]
        self.s = [int(x) if x != '' else None for x in ss]

parser = argparse.ArgumentParser(description="Write region and particle data to files for visualization")
parser.add_argument("--replicate", '-R', metavar='REPLICATE_INDEX', help='Replicate to render', dest="replicate", default=1, type=int)
parser.add_argument("--frames", '-F', metavar='PYTHON_SLICE', help='Range of frames to use' , dest="frames", default=FrameSlice(-1), type=FrameSlice)
parser.add_argument('input', metavar='YAML_FILE', type=str, help='YAML configuration file')
parser.add_argument("--logging-level", "-L", dest='logLevel', help="Logging level: CRITICAL, ERROR, WARNING, INFO, DEBUG", metavar="LEVEL", type=str, default="INFO")
parser.add_argument("--tcl-output", "-T", dest='tcl', action='store_true', help="Write mesh to tcl script files")
parser.add_argument("--json-output", "-J", dest='json', action='store_true', help="Write mesh and particle positions to json file")
parser.add_argument("--povray-output", "-P", dest='pov', action='store_true', help="Write mesh and particle positions to povray files")
parser.add_argument("--ascii-output", "-A", dest='ascii', action='store_true', help="Write particle positions to text files")
parser.add_argument("--no-spheres", "-s", dest='spheres', action='store_false', help="Disable species output")
parser.add_argument("--no-regions", "-r", dest='regions', action='store_false', help="Disable region mesh output")
parser.add_argument("--find-species", "-f", dest='find', type=str, default=None, help="Find frame where species count is maximized")
parser.add_argument("--prefix", "-p", dest='prefix', type=str, default='', help='Output file prefix')
parser.add_argument("--accumulate", "-a", dest='acc', type=int, default=1, help="Collect enough frames to ensure found species count")
args = parser.parse_args()

level = getattr(logging, args.logLevel.upper(), None)
if not isinstance(level, int):
    raise ValueError('Invalid log level: {}'.format(args.logLevel))

logging.basicConfig(level=level, format='%(levelname)s) %(module)s - %(message)s',)
log = logging.getLogger(__name__)

if sum( getattr(args,v) for v in ['tcl', 'json', 'pov', 'ascii'] ) == 0:
    log.error("Must specify an output format")
    exit(-1)


cfgData = yaml.load(open(args.input))

if args.spheres:
    speciesCfg = cfgData['species']
else:
    speciesCfg = dict()
if args.regions:
    regionsCfg = cfgData['regions']
else:
    regionsCfg = dict()

lmFileName = cfgData['lmFile']

def outputFile(x,idx=None,ext="pov"):
    if idx.__class__ == int:
        return "{}{}-{}_{:06d}.{}".format(args.prefix,cfgData['baseName'], x,idx,ext)
    else:
        return "{}{}-{}.{}".format(args.prefix,cfgData['baseName'], x,ext)

# read from lattice and store data as x,y,z tuples

with h5py.File(lmFileName, "r") as hdf:
    print(lmFileName)
    particleMap = { s:i+1 for i,s in enumerate(hdf['Parameters'].attrs['speciesNames'].decode().split(',')) }

    if args.find is not None and args.acc == 1:
        if args.frames.s == [-1,None,None]:
            args.frames = FrameSlice(":")

        bestct = 0
        fidxs = numpy.array(list(set(v-1 for s,v in particleMap.items() if re.match(args.find, s) is not None)))
        assert(len(fidxs) > 0)

        for repStr, rep in hdf['Simulations'].items():
            hdfData = rep['SpeciesCounts']
            cts = numpy.zeros(hdfData.shape, dtype=hdfData.dtype)
            cts_ = numpy.zeros(hdfData.shape, dtype=hdfData.dtype)
            hdfData.read_direct(cts_)
            cts[args.frames.slice] =   cts_[args.frames.slice]

            cts = numpy.sum(cts[args.frames.slice,fidxs], axis=1)
            idx = numpy.argmax(cts)
            ct = cts[idx]

            if ct > bestct:
                frame = "{:010d}".format(idx)
                replicate = repStr
                bestct = ct
        if bestct == 0:
            log.error("{} is never present".format(args.find))
            exit()
        else:
            log.info("found {} of {} at {}:{}".format(bestct, args.find, replicate,frame))
            frames = [frame]
    elif args.find is not None and args.acc > 1:
        accList = []
        if args.frames.s == [-1,None,None]:
            args.frames = FrameSlice(":")

        runningCt = 0
        fidxs = numpy.array(list(set(v-1 for s,v in particleMap.items() if re.match(args.find, s) is not None)))
        assert(len(fidxs) > 0)

        for repStr, rep in hdf['Simulations'].items():
            hdfData = rep['SpeciesCounts']
            cts = numpy.zeros(hdfData.shape, dtype=hdfData.dtype)
            cts_ = numpy.zeros(hdfData.shape, dtype=hdfData.dtype)
            hdfData.read_direct(cts_)

            fs =(numpy.arange(cts.shape[0])[args.frames.slice])
            random.shuffle(fs)

            cts[args.frames.slice,:] =   cts_[args.frames.slice,:]

            cts = cts[fs,:]
            cts = cts[:,fidxs]
            cts = numpy.sum(cts, axis=1)
            ctMask = cts>0
            fs = fs[ctMask]
            cts = cts[ctMask]

            cts = numpy.cumsum(cts)
            c=0
            for i,c in enumerate(cts):
                if runningCt+c > args.acc:
                    accList.append((repStr,fs[:i+1]))
                    runningCt+=c
                    break
            else:
                accList.append((repStr,fs))
                runningCt+=c

            if runningCt > args.acc:
                log.info("found {} of {}".format(runningCt, args.find))
                break
        else:
            log.warning("only found {} of {}".format(runningCt, args.find))

    else:
        replicate = '{:07d}'.format(args.replicate)

        try:
            nFrames = len(hdf['Simulations'][replicate]['Lattice'])
        except KeyError:
            nFrames = 0

        frames = ["{:010d}".format(x) for x in range(nFrames)[args.frames.slice]]

    siteName = { v['siteType']: x for x,v in regionsCfg.items()}
    selectedSites = set(siteName.keys())

    particleName = { y:x for x,y in particleMap.items()}
    particleName[0] = '__empty__'

    diffusionModel = hdf['Model']['Diffusion']
    latticeDim = tuple(int(diffusionModel.attrs['lattice'+i+'Size'][0]) for i in "XYZ")

    with open(outputFile("dim"),"w") as f:
        fmt = "#declare latticeDim{} = <{},{},{}>;"
        print(fmt.format(0,0,0,0), file=f)
        print(fmt.format(1,latticeDim[0],latticeDim[1],latticeDim[2]), file=f)

    latticeSpacing = diffusionModel.attrs['latticeSpacing'][0]
    pps = diffusionModel.attrs['particlesPerSite'][0]

    sites,speciesFrames = dict(), []

    if args.regions:
        sites = {x:[] for x in regionsCfg}

        hdfData = diffusionModel['LatticeSites']
        siteLattice = numpy.zeros(hdfData.shape, dtype=hdfData.dtype)
        hdfData.read_direct(siteLattice)

        for i,j,k in itertools.product(range(latticeDim[0]),range(latticeDim[1]),range(latticeDim[2])):
            s = siteLattice[i,j,k]
            if s in selectedSites:
                sites[siteName[s]].append((i,j,k))
        for s in sites:
            log.debug("{}: {} sites".format(s, len(sites[s])))
        log.debug("Finished parsing site types")



    if args.spheres and args.acc==1:
        speciesSets = { name: set(s for s in particleMap if re.match(d['regex'], s) is not None) for name,d in speciesCfg.items() }
        speciesFrames = []
        hdfData = hdf['Simulations'][replicate]['Lattice'][frames[0]]
        speciesLattice = numpy.zeros(hdfData.shape, dtype=hdfData.dtype)
        for frame in frames:
            hdfData = hdf['Simulations'][replicate]['Lattice'][frame]
            hdfData.read_direct(speciesLattice)

            species = {x:[] for x in speciesCfg}
            fct = 0.1
            for i,j,k in itertools.product(range(latticeDim[0]),range(latticeDim[1]),range(latticeDim[2])):
                sps = speciesLattice[i,j,k,:]
                spsCt = 0

                for spIdx,(sp,ss) in itertools.product(sps, speciesSets.items()):
                    if particleName[spIdx] in ss:
                        # unique hash for each particle ignoring position in species list
                        # but keeping track of its ordering within it
                        spsHash = i + j*latticeDim[0] + k*latticeDim[0]*latticeDim[1] \
                                + spIdx*latticeDim[0]*latticeDim[1]*latticeDim[2] \
                                + spsCt*latticeDim[0]*latticeDim[1]*latticeDim[2]*pps

                        species[sp].append((i+0.5,j+0.5,k+0.5,spsHash))
                        spsCt += 1
                if i*j*k/latticeDim[0]/latticeDim[1]/latticeDim[2] > fct:
                    fct += 0.1
                    log.debug("{} {:.2g} complete".format(frame, i*j*k/latticeDim[0]/latticeDim[1]/latticeDim[2]))
            for sp,vs in species.items():
                if 'thin' in speciesCfg[sp]:
                    vs[:] = random.sample(vs, int(0.5+len(vs)*speciesCfg[sp]['thin']))
            log.debug("Loaded frame {}".format(frame))
    elif args.spheres and args.acc>1:
        speciesSets = { name: set(s for s in particleMap if re.match(d['regex'], s) is not None) for name,d in speciesCfg.items() }
        speciesFrames = []
        species = {x:[] for x in speciesCfg}
        totalFrames = sum( len(f) for _,f in accList)
        fct = 0
        for replicate,fidxs in accList:
            hdfData = hdf['Simulations'][replicate]['Lattice']['0000000000']
            speciesLattice = numpy.zeros(hdfData.shape, dtype=hdfData.dtype)
            for fidx in fidxs:
                fct+=1
                frame = "{:010d}".format(fidx)
                log.info("frame: {}:{} {:>10d}/{:<10d}".format(replicate,frame,fct,totalFrames))
                hdfData = hdf['Simulations'][replicate]['Lattice'][frame]
                hdfData.read_direct(speciesLattice)

                for i,j,k in itertools.product(range(latticeDim[0]),range(latticeDim[1]),range(latticeDim[2])):
                    sps = speciesLattice[i,j,k,:]
                    spsCt = 0

                    for spIdx,(sp,ss) in itertools.product(sps, speciesSets.items()):
                        if particleName[spIdx] in ss:
                            # unique hash for each particle ignoring position in species list
                            # but keeping track of its ordering within it
                            spsHash = i + j*latticeDim[0] + k*latticeDim[0]*latticeDim[1] \
                                    + spIdx*latticeDim[0]*latticeDim[1]*latticeDim[2] \
                                    + spsCt*latticeDim[0]*latticeDim[1]*latticeDim[2]*pps

                            species[sp].append((i+0.5,j+0.5,k+0.5,spsHash))
                            spsCt += 1

            log.debug("Finished parsing species from rep %s", replicate)
    speciesFrames.append(species)


log.debug("Finished reading %s", lmFileName)

def addPoint(x,y):
        return (x[0]+y[0],x[1]+y[1],x[2]+y[2])

def subPoint(x,y):
        return (x[0]-y[0],x[1]-y[1],x[2]-y[2])

def cross(x,y):
    return (x[1]*y[2]-x[2]*y[1],
           -x[2]*y[0]+x[0]*y[2],
            x[0]*y[1]-x[1]*y[0])
   
def dot(x,y):
    return sum(a*b for a,b in zip(x,y))


# convert a triangle to a canonical form while preserving normal
def normalizeTriangle(t):
    _,i = min(zip(t,[0,1,2]))
    vs = [t[0],t[1],t[2],t[0],t[1]]
    return (vs[i],vs[i+1],vs[i+2])

# convert a triangle to a canonical form ignoring normal
def flattenTriangle(t):
    return tuple(sorted(t))


# return all 8 faces of a cube. returns list of faces, where faces 
# include a unique key describing that face and the two triangles
# thet compose the face
def getCubeFaces(v):
        # loop over xyz and append to list a unique key describing the face 
        # along with triangles making up the two parallel faces the cube in that direction

        fs = []
        for i in [0,1,2]:
            dr = [0,0,0]
            dr[i] = 1
            dl = [0,0,0]
            dl[(i+1)%3] = 1
            dd = [1,1,1]
            dd[(i+2)%3] = 0
            vv = addPoint(v,(1,1,1))

            # correct winding order later
            a,b,c,d = v, addPoint(v,dd), addPoint(v,dl), addPoint(v,dr)
            e,f,g,h = vv, subPoint(vv,dd), subPoint(vv,dl), subPoint(vv,dr)

            t1 = a, d, c
            t2 = b, c, d
            t3 = e, h, g
            t4 = f, g, h

            key1 = flattenTriangle([a,b,c,d])
            key2 = flattenTriangle([e,f,g,h])

            fs.append( (key1,t1,t2) )
            fs.append( (key2,t3,t4) )

        return fs



# Given a list of trangles, starting triangle to work with, its normal vector, and the correct normal vector, 
# iterate over all triangles to make their winding order self consistant 
def fixWindingOrder(region,triangles,initTriangle,triNormal, correctNormal):
    class Ct:
        pass
    flipCt = Ct()
    flipCt.v = 0
    edgeTriangleDict = dict()
    initTriangle = normalizeTriangle(initTriangle)

    nTriangles = len(triangles)

    for tri in triangles:
        a,b,c = tri
        for e in [(a,b), (a,c), (min(b,c), max(b,c))]:
            if e not in edgeTriangleDict:
                edgeTriangleDict[e] = [tri]
            else:
                edgeTriangleDict[e].append(tri)

    def trisFromEdge(e): return edgeTriangleDict[min(e),max(e)]
    def parity(x):       return 1 if x in [(0,1), (1,2), (2,0)] else -1
    def invertParity(x): return normalizeTriangle([x[0],x[2],x[1]])

    if dot(correctNormal, triNormal) < 0:
        tri = invertParity(initTriangle)
        flipCt.v += 1
    else:
        tri = initTriangle

    newInitTriangle = tri

    newTriangles = [tri]
    visited = set([initTriangle])

    def rectifyTriangle(tri, lastTriangle):
        # adjacent triangles must traverse their shared verticies in opposite directions
        # check for equal parity of edge permutation indicies and reverse if necessary

        edges = [v for v in tri if v in lastTriangle ]
        edgeOrder = tuple( tri.index(i) for i in edges )
        lastEdgeOrder = tuple( lastTriangle.index(i) for i in edges )

        if parity(lastEdgeOrder) == parity(edgeOrder):
            flipCt.v += 1
            tri = invertParity(tri)

        return tri

    def adjacentTri(t):
        for e in [[t[0],t[1]],[t[1],t[2]],[t[2],t[0]]]:
            for tri in trisFromEdge(e):
                yield tri

    queue = [ (x,tri) for x in adjacentTri(initTriangle) ]

    # main loop, work consists of [ (triangle to test, parent triangle that has been accepted as correct), ...]
    while len(queue) > 0:
        tri, lastTri = queue.pop()
        if tri not in visited:
            newTri = rectifyTriangle(tri, lastTri)
            newTriangles.append(newTri)
            visited.add(tri)
            for x in adjacentTri(tri):
                if x not in visited:
                    queue.append((x,newTri))
        if len(newTriangles)%50000 == 0:
            log.debug("{}: {}/{}".format(region,len(newTriangles),nTriangles))

    return newTriangles, flipCt.v, newInitTriangle

# compute region meshes

meshVert, meshTri = dict(), dict()

for region,ss in sites.items():
    faces = dict()

    nFaces0, nFaces1 = 0, 0

    # accumulate all exterior faces from lattice sites

    def addFace(x):
        # if two faces exist in the same position, they are interior faces
        #  --> ignore them

        key,tri1,tri2 = x

        if key in faces:
            faces[key] = None
            return 0
        else:
            faces[key] = (tri1, tri2)
            return 1

    for v in ss:
        for f in getCubeFaces(v):
            nFaces0 += 1
            nFaces1 += addFace(f)


    faces = [(c[0],c[1]) for c in faces.values() if c is not None]
   
    # convert triangular faces to face-vertex mesh
    # normalizeTriangle is to insure proper keying of lookups
    verts = set()
    for (a,b,c),(d,_,_) in faces:
        verts.add(a)
        verts.add(b)
        verts.add(c)
        verts.add(d)
    verts = sorted(list(verts))
    indexFromVert = { f: i for i,f in enumerate(verts) }
    triangles = [ normalizeTriangle([indexFromVert[v] for v in tri]) for face in faces for tri in face ]

    
    # fix winding order

    newTriangles, normalSgn, flipCt = [], -1, 0

    # inner loop is over contiguous regions of triangles, such as an inner mesh and an outer mesh
    while len(newTriangles) != len(triangles):
        ntset = set( flattenTriangle(t) for t in newTriangles)

        toDo = [ t for t in triangles if flattenTriangle(t) not in ntset ]

        if len(toDo) > 0:
            # get an external triangle: whatever is the closest to the x origin will be
            # pointing toward -x by construction.
            initTriangle = min(toDo, key=lambda idxs: sum( verts[j][0] for j in idxs ) )
            ct = -1
            itr = 1

            # run fixWinding order until it converges
            while ct != 0:
                initNormal =  cross(subPoint(verts[initTriangle[1]],
                                             verts[initTriangle[0]]),
                                    subPoint(verts[initTriangle[2]],
                                             verts[initTriangle[1]]))
                toDo, ct, initTriangle = fixWindingOrder("{}{:02d}".format(region,itr),toDo, initTriangle, initNormal, (normalSgn,0,0) )
                flipCt += ct
                itr+=1
                if itr == 10:
                    log.warning("Region `{}' failed to converge to a consistent triangle orientation".format(region))
                    break

            newTriangles += toDo
            normalSgn = -normalSgn # assuming concentric surfaces

    log.debug("{} quad count: {} -> {}; flipped {} triangles".format(region, nFaces0, nFaces1,flipCt))

    meshVert[region] = verts
    meshTri[region] = newTriangles

        
for region in sites:
    if args.tcl:
        fname = outputFile(region,ext='tcl')
        with open(fname, "w") as tcl:
            def vec(v): # conversion to angstroms
               return "{{{} {} {}}}".format(*[1e10*latticeSpacing*x for x in meshVert[region][v]])
            for a,b,c in meshTri[region]:
                print("draw triangle {} {} {}".format(vec(a), vec(b), vec(c)), file=tcl)
            tcl.close()
        log.debug("tcl output: " + fname)

            
    if args.pov:
        fname = outputFile(region,ext='pov')
        with open(fname, "w") as pov:
            print("vertex_vectors {", file=pov)
            nlast = len(meshVert[region])-1
            print("{},".format(nlast+1),file=pov)
            for i,v in enumerate(meshVert[region]):
                if i != nlast:
                    fmt = r'<{},{},{}>,'
                else:
                    fmt = r'<{},{},{}>'
                print(fmt.format(*v), file=pov, end='')
                if i%6 == 5:
                    print(file=pov)
            print("}", file=pov)

            print("face_indices {", file=pov)
            nlast = len(meshTri[region])-1
            print("{},".format(nlast+1),file=pov)
            for i,(a,b,c) in enumerate(meshTri[region]):
                if i == nlast:
                    fmt = "<{},{},{}>"
                else:
                    fmt = "<{},{},{}>, "
                print(fmt.format(a,b,c), file=pov, end='')
                if i%6 == 2:
                    print(file=pov)
            print("}", file=pov)
        log.debug("pov output: " + fname)


def rv():
    return  2*random.random() - 1

for fIdx,species in enumerate(speciesFrames):
    if args.ascii:
        for sp, loc in species.items():
            if len(speciesFrames) == 1:
                fname = outputFile(sp,ext="dat")
            else:
                fname = outputFile(sp,fIdx,ext="dat")
            with open(fname, "w") as txt:
                for v in loc:
                    # add noise deterministically, offsets will not change over different frames

                    r = speciesCfg[sp]['radius']
                    noiseStrength= cfgData['globalNoiseStrength']*max(0.5-r, 0)
                    random.seed(v[3])
                    coord = [ x+noiseStrength*rv() for x in v[:3] ]

                    print("<{:.4e}, {:.4e}, {:.4e}>, {:.4e},".format(coord[0],coord[1],coord[2],r), file=txt)
                        
            log.debug("ascii output: wrote {} spheres of type {} to {}".format(len(loc), sp, fname))

    if args.pov:
        for sp, loc in species.items():
            if len(speciesFrames) == 1:
                fname = outputFile(sp)
            else:
                fname = outputFile(sp,fIdx)
            with open(fname, "w") as pov:
                for v in loc:
                    # add noise deterministically, offsets will not change over different frames

                    r = speciesCfg[sp]['radius']
                    noiseStrength= cfgData['globalNoiseStrength']*max(0.5-r, 0)
                    random.seed(v[3])
                    coord = [ x+noiseStrength*rv() for x in v[:3] ]

                    fmt2 =  "<{:.4e}, {:.4e}, {:.4e}>"
                    fmt3 =  "sphere {{ {}, {} }}"
                    vstr = fmt2.format(*coord)
                    print(fmt3.format(vstr, r), file=pov)
                        
            log.debug("pov output: wrote {} spheres of type {} to {}".format(len(loc), sp, fname))


if args.json:
    jsonRoot = dict()
    jsonRoot['renderCfgData'] = cfgData
    jsonRoot['latticeDim'] = latticeDim
    jsonRoot['latticeSize'] = latticeSpacing
    jsonRoot['meshVert'] = meshVert
    jsonRoot['meshTri'] = meshTri
    jsonRoot['species'] = []
    for fIdx,species in enumerate(speciesFrames):
        frame = dict()
        for sp, loc in species.items():
            locs = []
            for v in loc:
                elem = dict(pos=v[:3], hash=int(v[3]))
                locs.append(elem)
            frame[sp] = locs
        jsonRoot['species'].append(frame)

    fname = cfgData['baseName'] + ".json"
    json.dump(jsonRoot, open(fname, "w"), indent=3)
    log.debug("json output: " + fname)

