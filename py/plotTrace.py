#!/usr/bin/python

import argparse, itertools, h5py, yaml, re, logging, numpy, random, json, glob
import matplotlib.pylab as plt

class Slicer:
    @property
    def slice(self):
        return slice(*[int(x) if x != '' else None for x in self.ss])

    def __init__(self, s):
        s = str(s)
        ss = s.split(':')
        ss += ['', '', '']
        self.ss = ss[:3]

parser = argparse.ArgumentParser(description="Plot species counts vs time")
parser.add_argument("--replicate", '-R', metavar='REPLICATE_INDEX', help='Replicate to render', dest="replicate", default=1, type=int)
parser.add_argument("--replicate-mean", '-M', help='compute mean over all replicates', dest="replicateMean", action='store_true')
parser.add_argument("--frames", '-F', metavar='PYTHON_SLICE', help='Range of frames to use' , dest="frames", default=Slicer(":"), type=Slicer)
parser.add_argument('input', metavar='LM_FILE', type=str, help='Lattice microbes input file')
parser.add_argument("--logging-level", "-L", dest='logLevel', help="Logging level: CRITICAL, ERROR, WARNING, INFO, DEBUG", metavar="LEVEL", type=str, default="INFO")
parser.add_argument("--log", dest='logAxes', help="Plot logscale for x, xy, or y.", type=str, default=None)
parser.add_argument("--minutes", '-m', dest='minutes', help="Plot logscale for x, xy, or y.", action='store_true')

parser.add_argument('output', metavar='MPL_FILE', type=str, help='Matplotlib output')
parser.add_argument('speciesSpec', metavar='NAME:REGEX', type=str, help='Name of species to plot : regex selecting those species', nargs="+")
args = parser.parse_args()

level = getattr(logging, args.logLevel.upper(), None)
if not isinstance(level, int):
    raise ValueError('Invalid log level: {}'.format(args.logLevel))

logging.basicConfig(level=level, format='%(levelname)s) %(module)s - %(message)s',)
log = logging.getLogger(__name__)


if '*' in args.input or '?' in args.input:
    files = glob.glob(args.input)
else:
    files = [args.input]

print(files)

hdf = h5py.File(files[0])
names = hdf['Parameters'].attrs['speciesNames'].decode().split(',')
particleMap = { s:i for i,s in enumerate(names) }


integratedSpecies = []
speciesLists = []
for st in args.speciesSpec:
    try:
        name,regex = st.split(':')
        if name[-2:] == '/i':
            integrated = True
            name = name[:-2]
        else:
            integrated = False

        speciesLists.append((name, set(i for s,i in particleMap.items() if re.match(regex, s) is not None)))
        log.debug("'{}' ==> ({}) [integrated={}]".format(st, ', '.join(s for s in particleMap if re.match(regex, s) is not None), integrated))
        if integrated:
            integratedSpecies.append(name)
    except:
        log.error("species specification defined as SelectionName:RegexMatchingSpecies")
        raise


hdfData = hdf['Simulations']["{:07d}".format(args.replicate)]['SpeciesCountTimes']
ts = numpy.zeros(hdfData.shape, dtype=hdfData.dtype)
hdfData.read_direct(ts)

counts = numpy.zeros((hdfData.shape[0], len(names)))

hdf.close()

fNorm = 1.0/len(files)
for f in files:
    hdf = h5py.File(f)
    if args.replicateMean:
        rNorm = fNorm/len(hdf['Simulations'])
        for r in hdf['Simulations']:
            hdfData = hdf['Simulations'][r]['SpeciesCounts']
            tmp = numpy.zeros(hdfData.shape, dtype=hdfData.dtype)
            hdfData.read_direct(tmp)
            counts += tmp*rNorm
    else:
        try:
            hdfData = hdf['Simulations']["{:07d}".format(args.replicate)]['SpeciesCounts']
        except:
            print(f)
            print(list(hdf.keys()))
            raise
        tmp = numpy.zeros(hdfData.shape, dtype=hdfData.dtype)
        hdfData.read_direct(tmp)
        counts += tmp*fNorm
    hdf.close()

if args.minutes:
    ts /= 60;

log.info("Plotting over {} frames".format(ts.shape[0]))

fig = plt.figure(0,(6,4))


for name,cts in speciesLists:
    ys = numpy.zeros(ts.shape, dtype=counts.dtype)
    spMatches = dict()
    for i in cts:
        ys += counts[:,i]
        if any(counts[:,i] >0):
            try:
                spMatches[names[i]]  = max(spMatches[names[i]], counts[:,i].max())
            except KeyError:
                spMatches[names[i]]  = counts[:,i].max()

    if name in integratedSpecies:
        ysFilt = numpy.diff(ys)
        ysFilt[ysFilt < 0] = 0
        ys = numpy.array([0] + list(numpy.cumsum(ysFilt)) )



    log.debug("{} trace contains {}".format(name, ', '.join("{} ({})".format(*v) for v in spMatches.items())))

    if args.logAxes == 'x':
        pfn = plt.semilogx
    elif args.logAxes == 'y':
        pfn = plt.semilogy
    elif args.logAxes == 'xy':
        pfn = plt.loglog
    elif args.logAxes is None:
        pfn = plt.plot
    else:
        raise RuntimeError("WTF is {}?".format(args.logAxes))
   
    pfn(ts[args.frames.slice],ys[args.frames.slice],label=name)

    dYs = numpy.diff(ys)
    posDn = sum(dYs[dYs>0])
    negDn = -sum(dYs[dYs<0])
    t0,y0 = ts[0], ys[0]
    t1,y1 = ts[-1], ys[-1]
    log.info("{}: {} -> {} over [{:.3f},{:.3f}): total change  {}, +{}, -{}".format(name, y0, y1, t0,t1,posDn-negDn, posDn, negDn))

box = plt.gca().get_position()
plt.gca().set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.legend(loc='best',ncol=4)
# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.ylabel("Species count")
if args.minutes:
    plt.xlabel("$t$/min")
else:
    plt.xlabel("$t$/s")
plt.tight_layout()
plt.xlim((ts.min(),ts.max()))

plt.savefig(args.output)

