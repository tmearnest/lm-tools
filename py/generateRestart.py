#!/usr/bin/python

import argparse, h5py, numpy, logging, re, os, os.path
import matplotlib.pylab as plt

parser = argparse.ArgumentParser(description="Generate input files restarting a simulation")
parser.add_argument("--replicate", '-R', metavar='REPLICATEE', help='Replicate number to use', dest="replicate", default=1, type=int)
parser.add_argument("--frame", '-F', metavar='FRAME', help='Frame to use', dest="frame", default=-1, type=int)
parser.add_argument("--logging-level", "-l", dest='logLevel', help="Logging level: CRITICAL, ERROR, WARNING, INFO, DEBUG", metavar="LEVEL", type=str, default="INFO")
parser.add_argument("--force", "-f", dest='clobber', help="Clobber existing files", action='store_true')
parser.add_argument("--final-time", "-t", dest='finalTime', help="Set final time instead of inferring from input", type=float, default=None)
parser.add_argument('files', metavar='LM_SRC', type=str, help='HDF5 simulation files', nargs='+')
args = parser.parse_args()

level = getattr(logging, args.logLevel.upper(), None)
if not isinstance(level, int):
    raise ValueError('Invalid log level: {}'.format(args.logLevel))

logging.basicConfig(level=level, format='%(levelname)s) %(module)s - %(message)s',)
log = logging.getLogger(__name__)


newFiles = []
for f in args.files:
    m = re.search('(.*)\.(\d+)\.lm', f)
    if m is not None:
        newFile = "{}.{:06d}.lm".format(m.group(1),int(m.group(2))+1)
    else:
        m = re.search('(.*)\.lm', f)
        if m is not None:
            newFile ="{}.{:06d}.lm".format(m.group(1),1)
        else:
            log.error("Got a weird filename: {}. rename to asdfsadf.lm".format(f))
            exit(-1)
    if os.path.exists(newFile):
        if args.clobber:
            os.remove(newFile)
        else:
            log.error("Output file {} exists.".format(newFile))
            exit(-1)

    newFiles.append(newFile)

replicate = "{:07d}".format(args.replicate)
        
for nName, oName in zip(newFiles,args.files):
    with h5py.File(nName, userblock_size=512) as newHdf, h5py.File(oName) as oldHdf:
        oldSim = oldHdf['Simulations'][replicate]

        frame = list(oldSim['Lattice'].keys())[args.frame]
        oldLattice = oldSim['Lattice'][frame].value
        oldLatticeTime = oldSim['LatticeTimes'][args.frame]
        oldFinalTime = float(oldHdf['Parameters'].attrs['maxTime'])


        speciesCounts = numpy.zeros(oldHdf['Model']['Reaction'].attrs['numberSpecies'])
        for j in oldLattice.ravel():
            if j > 0:
                speciesCounts[j-1] += 1


        oldHdf.copy('Model', newHdf)
        oldHdf.copy('Parameters', newHdf)
        newHdf.create_group("Simulations")
        
        newLattice = newHdf['Model']['Diffusion']['Lattice']
        newLattice.write_direct(oldLattice)
        newCounts = newHdf['Model']['Reaction']['InitialSpeciesCounts']
        newCounts.write_direct(speciesCounts)

        if args.finalTime is None:
            finalTime = oldFinalTime - oldLatticeTime
        else:
            finalTime = args.finalTime 

        newHdf['Parameters'].attrs['maxTime'] = numpy.string_("{:.3f}".format(finalTime)) #wtf is this a string??
        newHdf.attrs['formatVersion'] = oldHdf.attrs['formatVersion']

        log.info("new LM file {} from {}:{}:{}, restart @ {:.2f} s".format(nName,oName,replicate,frame,oldLatticeTime))
    f = open(nName, "r+b")
    f.write(b'LMH5')
    f.close()


