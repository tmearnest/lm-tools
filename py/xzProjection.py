#!/usr/bin/python

import argparse, itertools, h5py, numpy, re, logging, subprocess, functools
import matplotlib.pylab as plt
from matplotlib.path import Path
import matplotlib.patches as patches
from lxml import etree

parser = argparse.ArgumentParser(description="Compute xz projection of particle density")
parser.add_argument("--replicates", '-R', metavar='REPLICATE_RANGE', help='Range of replicates to use', dest="replicates", default='-', type=str)
parser.add_argument("--figs-per-page", '-N', metavar='N', help='Number of figures on each page', dest="figsPerPage", default=4, type=int)
parser.add_argument("--logging-level", "-L", dest='logLevel', help="Logging level: CRITICAL, ERROR, WARNING, INFO, DEBUG", metavar="LEVEL", type=str, default="INFO")
parser.add_argument("--site-types", "-S", dest='boundaries', help="Site types to highlight", metavar="IDX0,IDX1,...", type=str, default='')
parser.add_argument("--site-type-colors", "-b", dest='boundaryColors', help="Color of site types to highlight", metavar="C0,C1,...", type=str, default='')
parser.add_argument("--y-slice", "-Y", dest='ySlice', help="Range of y-slices to average over (defaults to all)", metavar="START:END", type=str, default=None)
parser.add_argument('--species', '-s', dest='species', metavar='NAME0:REGEX0,NAME1:REGEX1,...', type=str, help='Name of species to plot : regex selecting those species', default=None)
parser.add_argument('--skip-empty', '-E', dest='skipEmpty', help='Do not plot empty histograms', action='store_true')
parser.add_argument('--concatenate-pages', '-P', dest='catPdfs', help='Bind all plots into single PDF', action='store_true')
parser.add_argument('--no-title', '-T', dest='titleFig', help='Do not place species title on figures', action='store_false')
parser.add_argument('--correct-projection-sampling', '-C', dest='correctSampling', help='Divide by number of sites at each xz-point', action='store_true')
parser.add_argument('--color-map', '-m', dest='colormap', help='Colormap for density plot', metavar='MPL_COLORMAP', default='jet')
parser.add_argument('--no-blank-default', '-B', dest='blankDefault', help='Do not cover default region with white', action='store_false')
parser.add_argument('--mpl-output-format', '-O', dest='mplOutputFormat', metavar='MPL_EXT', help="Output extension that matplotlib knows about, e.g. svg", default='pdf')
parser.add_argument('input', metavar='LM_FILE', type=str, help='HDF5 simulation output file from LM')
parser.add_argument('histogram', metavar='NPY_FILE', type=str, help='Histogram data')
parser.add_argument('basename', metavar='MPL_FILE_BASENAME', type=str, help='Plot projected distribution')
args = parser.parse_args()

level = getattr(logging, args.logLevel.upper(), None)
if not isinstance(level, int):
    raise ValueError('Invalid log level: {}'.format(args.logLevel))

logging.basicConfig(level=level, format='%(levelname)s) %(module)s - %(message)s',)
log = logging.getLogger(__name__)

if args.catPdfs and args.mplOutputFormat != 'pdf':
    log.error("Need to write pdf output in order to concatenate them.")
    exit(-1)


with h5py.File(args.input, "r") as hdf:
        names = hdf['Parameters'].attrs['speciesNames'].decode().split(',')
        diffusionModel = hdf['Model']['Diffusion']
        latticeSpacing =  float(diffusionModel.attrs['latticeSpacing'])
        latticeDim = tuple(int(diffusionModel.attrs['lattice'+i+'Size'][0]) for i in "XYZ")

        if args.species is not None:
            speciesLists = []
            for st in args.species.split(','):
                try:
                    name,regex = st.split(':')
                    sps =  set(i for i,s in enumerate(names) if re.match(regex, s) is not None)
                    speciesLists.append( (name, sps) )
                    assert(len(sps) > 0)
                except:
                    log.error("species specification defined as SelectionName:RegexMatchingSpecies: failed on '{}'".format(st))
                    print(names)
                    raise
            for n,ss in speciesLists:
                log.debug("{} matches: [{}]".format(n, ', '.join(names[s] for s in ss)))
        else:
            speciesLists = sorted( (s,set((i,))) for i,s in enumerate(names))



        hdfData = diffusionModel['LatticeSites']
        siteTypeLattice = numpy.zeros(hdfData.shape, dtype=hdfData.dtype)
        nRegions = int(hdf['Model']['Spatial']['Regions'].attrs['numberRegions'])
        hdfData.read_direct(siteTypeLattice)


if args.ySlice is not None:
    if ':' in args.ySlice:
        yIdx0,yIdx1 = ( int(x) for x in args.ySlice.split(':') )
    else:
        yIdx0 = int(args.ySlice)
        yIdx1 = yIdx0+1
else:
    yIdx0, yIdx1 = 0, latticeDim[1]

assert( 0 <= yIdx0 <= latticeDim[1])
assert( 0 <= yIdx1 <= latticeDim[1])

ctrSlice = (yIdx0+yIdx1)//2


def getBoundaryPath(siteType, siteSlice):

    # collect edges of each lattice site
    # only keep boundary edges, if more than one edge traverses two points, it is internal

    faces = dict()
    def ap(*v):
        k = tuple(int(x) for x in v)
        if k in faces:
            faces[k] = None
        else:
            faces[k] = 1

    for x,y in itertools.product(*map(range,siteSlice.shape)):
        if siteSlice[x,y] == siteType:
            ap( x,y, x+1,y )
            ap( x,y, x,y+1 )
            ap( x,y+1, x+1,y+1 )
            ap( x+1,y, x+1,y+1 )


    faces2 = set((f[:2],f[2:]) for f,v in faces.items() if v is not None )

    # build map for looking up an edge based on its verticies
    vertexTable = { x:[] for x in  set(x for x,_ in faces2) | set( x for _,x in faces2) }

    for l,r in faces2:
        vertexTable[l].append((l,r))
        vertexTable[r].append((l,r))

    # merge edges into single closed path
    face = faces2.pop()
    paths = [ [ face[0], face[1] ] ]

    def faceEq(f0,f1):
        return (f0[0] == f1[0] and f0[1] == f1[1]) or (f0[1] == f1[0] and f0[0] == f1[1]) 

    while len(faces2) > 0:
        firstFace = paths[0][-2:]
        lastFace = paths[-1][-2:]
        pl, pr = lastFace
        for face in vertexTable[pr]:
            # append next vertex, making sure it's not already visited
            if not faceEq(face, lastFace) and not faceEq(face, firstFace):
                try:
                    vertexTable[pr].remove(face)
                    faces2.remove(face)
                except KeyError:
                    break

                if pr == face[0]:
                    paths[-1].append(face[1])
                else:
                    paths[-1].append(face[0])

                break
        else: 
            # start a new path if we ran out of edges to connect
            if len(faces2) > 0:
                face = faces2.pop()
                paths.append([ face[0], face[1] ])


    oldNodes = sum( len(x) for x in paths)

    newPaths = []
    for path in paths:
        # remove colinear nodes
        newPath = []
        for v0,v1,v2 in zip([path[-1]]+path,path,path[1:]+path):
            if (v0[0] == v1[0] and v1[0] == v2[0]) or (v0[1] == v1[1] and v1[1] == v2[1]):
                continue
            else:
                newPath.append(v1)

        # newPath = path[:]

        # set consistent winding order
        p0,p1 = newPath[0:2]
        if p0[0] == p1[0]:
            if p0[1] > p1[1]:
                newPath = newPath[::-1]
        else:
            if p0[0] > p1[0]:
                newPath = newPath[::-1]

        newPaths.append(newPath)

    newNodes = sum( len(x) for x in newPaths)

    log.debug("siteType {}: {} disjoint paths, reduced node count from {} to {}.".format(siteType, len(newPaths), oldNodes,newNodes))

    s =  1e6*latticeSpacing

    # save path in format mpl can deal with

    verts, codes = [], []

    # reverse sort by arc length
    def pathKey(x):
        return -sum((x0-x1)**2+(y0-y1)**2 for (x0,x1),(y0,y1) in zip(x,[x[-1]] + x[:-1]) )

    for pidx,path in enumerate(sorted(newPaths,key=pathKey)):
        vs, cs = [], []
        nVerts = len(path)
        for i,vertex in enumerate(path):
            vs.append( (s*vertex[1], s*vertex[0]) )
            if i == 0:
                cs.append( Path.MOVETO )
            else:
                cs.append( Path.LINETO )
        
        # assume longest path is external edge and reverse so that following paths subtract fill correctly
        # if there are "islands" inside cavities, they will have the wrong winding order.
        if pidx == 0:
            vs = vs[::-1]

        vs.append( (0,0) )
        cs.append( Path.CLOSEPOLY )
        verts += vs
        codes += cs

    return Path(verts,codes)

boundaryPaths = []
boundaryColors = []

siteTypeIdxs = set()
siteSlice = siteTypeLattice[:,ctrSlice,:]

if len(args.boundaries) > 0:
    boundaries = [ int(x) for x in args.boundaries.split(',') if len(x) > 0]
    boundaryColors = [x for x in args.boundaryColors.split(',') if len(x) > 0]
    if len(boundaryColors) != len(boundaries):
        boundaryColors = ['w']*len(boundaries)

    # build an ascii art representation of the boundaries in the slice 
    asciiMap = [ [' ' for _ in range(32)] for _ in range(128) ]
    for v0 in itertools.product(*map(range,siteSlice.shape)):
        siteTypeIdxs.add(siteSlice[v0])
        asciiMap[v0[1]][v0[0]] = ' 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'[siteSlice[v0]]

    log.debug("+{}+".format(len(asciiMap[0])*'-'))
    for l in asciiMap:
        log.debug("|{}|".format(''.join(l)))
    log.debug("+{}+".format(len(asciiMap[0])*'-'))


    # iterate over all site types to display
    boundaryPaths = [getBoundaryPath(st,siteSlice) for st in boundaries]

# get cell mask
cellMask = getBoundaryPath(0, siteSlice)

histograms = numpy.load(args.histogram)

# compute densities
siteCounts = { st: { sp: sum( histograms[sIdx,siteTypeLattice==st].sum() for sIdx in ss) for sp,ss in speciesLists} for st in siteTypeIdxs }
latticeSiteVol = latticeSpacing**3*1e18
regionVolume= { st: latticeSiteVol*(siteTypeLattice==st).sum() for st in siteTypeIdxs }
densities = { st: { sp: sum( histograms[sIdx,siteTypeLattice==st].sum() for sIdx in ss) for sp,ss in speciesLists} for st in siteTypeIdxs }


for st in siteTypeIdxs:
    for sp in densities[st]:
        densities[st][sp] /= regionVolume[st]


for sp,_ in speciesLists:
    log.info("{} stats:".format(sp))
    for st,dd in densities.items():
        log.info("   [{}]  {:.3g}/fL ({:.3g}/{:.3g} fL)".format(st, densities[st][sp], siteCounts[st][sp], regionVolume[st]))


extent = (0,latticeDim[2]*latticeSpacing*1e6,0,latticeDim[0]*latticeSpacing*1e6)
plt.figure(0,(6,2*args.figsPerPage))

log.debug("averaging y over {}:{}".format(yIdx0,yIdx1))

completedSps = []

plotFiles = []

def flushPlots(pageNum, sps):
        plt.tight_layout()
        fname = "{}-{:04d}.{}".format(args.basename, pageNum,args.mplOutputFormat)
        plt.savefig(fname)
        log.debug("{} plotted to {}".format(', '.join(completedSps),fname))
        if args.mplOutputFormat == 'svg':
            log.debug("{}: fixing image render mode".format(fname))
            svg = etree.parse(open(fname))
            for e in svg.iter("{http://www.w3.org/2000/svg}image"):
                e.attrib['image-rendering'] = "optimizeSpeed"
                log.debug('{}: image id={}: image-rendering="optimizeSpeed"'.format(fname,e.attrib['id']))
            open(fname,"w").write(etree.tostring(svg, pretty_print=True, xml_declaration=True, encoding='UTF-8').decode()) 

        plotFiles.append(fname)
        sps[:] = []

ct = 0
if args.correctSampling:
    samplingNorm = 0.01+numpy.sum(siteTypeLattice[:,yIdx0:yIdx1,:]>0, axis=1)
    log.debug("Normalizing by number of y sites")

for sp, ss in speciesLists:
    img = numpy.zeros( (latticeDim[0],latticeDim[2]) )
    for s in ss:
        img += numpy.sum(histograms[s][:,yIdx0:yIdx1,:], axis=1)

    if args.correctSampling:
        img /= samplingNorm

    if img.sum() == 0 and args.skipEmpty:
        log.debug("{}: 0 samples. Skipped.".format(sp))
        continue
        

    figNum, pageNum = ct%args.figsPerPage, ct//args.figsPerPage
    if figNum == 0:
        plt.clf()

    plt.subplot(args.figsPerPage,1,figNum+1)
    plt.imshow(img, cmap=args.colormap, extent=extent, interpolation='none', origin='lower')
    completedSps.append(sp)

    for p,c in zip(boundaryPaths,boundaryColors):
        plt.gca().add_patch(patches.PathPatch(p,fc='none',ec=c))

    if args.blankDefault:
        plt.gca().add_patch(patches.PathPatch(cellMask,fc='w',ec="w"))

    plt.ylabel(r"$x$-position $[\mu\mathrm{m}]$")
    plt.xlabel(r"$z$-position $[\mu\mathrm{m}]$")
    if args.titleFig:
        plt.title(sp.replace("_", r"\_"))

    if figNum == args.figsPerPage - 1:
        flushPlots(pageNum, completedSps)
    
    ct+=1

if figNum != args.figsPerPage - 1:
    flushPlots(pageNum, completedSps)

if args.catPdfs:
    fname = "{}.pdf".format(args.basename)
    log.debug("Calling ghostscript to write all figures to {}".format(fname))
    subprocess.call(['gs', '-dBATCH', '-dNOPAUSE', '-dColorConversionStrategy=/LeaveColorUnchanged', 
                       '-dEncodeColorImages=false', '-dEncodeGrayImages=false',  
                       '-dEncodeMonoImages=false','-sDEVICE=pdfwrite', '-q',
                       '-sOutputFile={}'.format(fname)] + plotFiles )

    
