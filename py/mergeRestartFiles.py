#!/usr/bin/python

import argparse, h5py, numpy, logging, re, os, os.path, shutil
import matplotlib.pylab as plt

parser = argparse.ArgumentParser(description="Combine restarted LM files")
parser.add_argument("--replicate", '-R', metavar='REPLICATEE', help='Replicate number to use', dest="replicate", default=1, type=int)
parser.add_argument("--logging-level", "-l", dest='logLevel', help="Logging level: CRITICAL, ERROR, WARNING, INFO, DEBUG", metavar="LEVEL", type=str, default="INFO")
parser.add_argument("--force", "-f", dest='clobber', help="Clobber existing files", action='store_true')
parser.add_argument('partials', metavar='LM_SRC', type=str, help='HDF5 partial simulation files', nargs='+')
parser.add_argument('output', metavar='LM_SRC', type=str, help='Merged HDF5 simulation file')
args = parser.parse_args()

level = getattr(logging, args.logLevel.upper(), None)
if not isinstance(level, int):
    raise ValueError('Invalid log level: {}'.format(args.logLevel))

logging.basicConfig(level=level, format='%(levelname)s) %(module)s - %(message)s',)
log = logging.getLogger(__name__)

if os.path.exists(args.output):
    if args.clobber:
        os.remove(args.output)
    else:
        log.error("Output file {} exists.".format(args.output))
        exit(-1)

replicate = "{:07d}".format(args.replicate)
shutil.copy(args.partials[0], args.output)

with h5py.File(args.output) as newHdf:
    del newHdf['Simulations']
    newHdf.create_group("Simulations")
    newHdf.create_group("Simulations/"+replicate)
    newHdf.create_group("Simulations/"+replicate+"/Lattice")
    tmpCounts = numpy.zeros(newHdf['Model']['Reaction'].attrs['numberSpecies'])
    latticeTimes,speciesCounts = None, None
    frameIdx = 0
    lastFrameTime = 0

    tmpLattice = newHdf['Model']['Diffusion']['Lattice']
    tmpLattice = numpy.zeros(tmpLattice.shape, dtype=tmpLattice.dtype)

    sim = newHdf['Simulations'][replicate]
    newLattice = sim['Lattice']

    firstFrame = True 

    for partialFname in args.partials:
        partialCount = 0
        with h5py.File(partialFname) as partialHdf:
            oldSim = partialHdf['Simulations'][replicate]

            ts = oldSim['LatticeTimes'].value + lastFrameTime

            if firstFrame:
                latticeTimes = ts
            else:
                ts = ts[1:] 
                latticeTimes = numpy.concatenate((latticeTimes, ts))

            lastFrameTime = ts[-1]

            for lct, lattice in enumerate(oldSim['Lattice'].values()):
                if lct == 0 and not firstFrame:   # first frame is a copy of the initial conditions
                    continue

                lattice.read_direct(tmpLattice)

                tmpCounts[:] = 0
                for x in tmpLattice.ravel():
                    if x > 0:
                        tmpCounts[x-1] += 1

                if firstFrame:
                    speciesCounts = numpy.array(tmpCounts)
                else:
                    speciesCounts = numpy.vstack((speciesCounts,tmpCounts))
                        
                d = newLattice.create_dataset("{:010d}".format(frameIdx), data=tmpLattice)

                frameIdx += 1
                partialCount += 1
                firstFrame = False

        log.info("Wrote {} frames from {}".format(partialCount, partialFname))

    d = sim.create_dataset("SpeciesCounts", data=speciesCounts)
    d = sim.create_dataset("SpeciesCountTimes", data=latticeTimes)
    d = sim.create_dataset("LatticeTimes", data=latticeTimes)

