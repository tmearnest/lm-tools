#!/usr/bin/python

import argparse, h5py, numpy, logging, random, shutil, re

parser = argparse.ArgumentParser(description="Use random selection of frames to generate initial conditions")
parser.add_argument("--logging-level", "-l", dest='logLevel', help="Logging level: CRITICAL, ERROR, WARNING, INFO, DEBUG", metavar="LEVEL", type=str, default="INFO")
parser.add_argument("--random-frame", "-F", dest='randomFrame', help="Use a random frame instead of last frame", action='store_true')
parser.add_argument("--frames-from-end", "-f", dest='frameStart', help="Sample starting conditions from only the last N frames", metavar="N", type=int, default=None)
parser.add_argument("--ensemble-size", "-N", dest='nEnsemble', help="Create N simulation files", metavar="N", type=int, default=1)
parser.add_argument("--limit-counts", "-L", dest='limitCounts', help="Ensure no species is present greater than N times", metavar="N", type=int, default=None)
parser.add_argument("--random-replicate", "-R", dest='randomReplicate', help="Use a random replicate instead of first", action='store_true')
parser.add_argument("--distribute", "-D", dest='distribute', metavar="SPS1,N1,I1,J1...:...", help="Distribute N species SPS over the compartments I,J,...", type=str, default=None)
parser.add_argument("--zero-out", "-Z", dest='zeroRegex', help="Set all matching species to zero", metavar='REGEX', default=None, type=str)
parser.add_argument('template', metavar='LM_TMPL', type=str, help='HDF5 simulation template file to build new simulations')
parser.add_argument('source', metavar='LM_SRC', type=str, help='HDF5 simulation file to take initial conditions from')
parser.add_argument('output', metavar='LM_FILE', type=str, help='HDF5 simulation output file')
parser.add_argument('replacement', metavar='S/PATTERN/REPL/', type=str, help='Sed style replacement. NB: use python re syntax', nargs='*')

args = parser.parse_args()

level = getattr(logging, args.logLevel.upper(), None)
if not isinstance(level, int):
    raise ValueError('Invalid log level: {}'.format(args.logLevel))

logging.basicConfig(level=level, format='%(levelname)s) %(module)s - %(message)s',)
log = logging.getLogger(__name__)

def outputFile(ct):
    if args.nEnsemble == 1:
        return args.output
    else:
        splitPath = args.output.split('.')
        return "{}.{:04d}.{}".format('.'.join(splitPath[:-1]),ct,splitPath[-1])

def doSedRep(st):
    st0 = st
    for cmd in args.replacement:
        if (cmd[0] != 's' and cmd[0] != 'S') or sum(1 for c in cmd if c==cmd[1]) != 3:
            log.error("invalid replacement syntax: '{}'".format(cmd))
            exit(-1)

        _,pat,rep,_ = cmd.split(cmd[1])
        st = re.sub(pat, rep, st)
    match = st != st0
    return match,st


with h5py.File(args.source, "r") as hdf:
        maxReplicate = max( int(r) for r in hdf['Simulations'].keys() )
        sourceNames = hdf['Parameters'].attrs['speciesNames'].decode().split(',')
        sourceMap = { s:i+1 for i,s in enumerate(sourceNames) }

with h5py.File(args.template, "r") as hdf:
        spNames = hdf['Parameters'].attrs['speciesNames'].decode().split(',')
        destMap = { s:i+1 for i,s in enumerate(spNames) }
        maxSpsIdx = max(max(destMap.values()),max(sourceMap.values()))
        translationTable = numpy.zeros(maxSpsIdx+1,dtype=numpy.int)
        if args.zeroRegex is not None:
            zeroSps = set( sp for sp in spNames if re.match(args.zeroRegex, sp) is not None )
            if len(zeroSps) == 0:
                log.warning('The regex "{}" failed to match any species names to zero out'.format(args.zeroRegex))
        else:
            zeroSps = set()

        failures, strings = [], []
        one2one = True

        for sp in set(sourceMap.keys()) | set(destMap.keys()):

            if sp in zeroSps:
                srcSp = sp
                destSp = ''
                target = '0'
                print("zeroed: {}".format(sp))
            else:
                if sp in sourceMap:
                    srcSp = sp
                    matched, destSp = doSedRep(sp)

                    if matched:
                        if destSp not in destMap:
                            failures.append( ( srcSp,destSp) )
                else:
                    destSp = sp
                    srcSp = ''

                if srcSp in sourceMap and destSp in destMap:
                    translationTable[sourceMap[sp]] = destMap[destSp]
                elif srcSp not in sourceMap:
                    one2one = False
                    srcSp = ''
                elif destSp not in destMap:
                    translationTable[sourceMap[srcSp]] = 0
                    destSp = ''
                    one2one = False

                target = destSp

            strings.append( (srcSp, target))

        if not one2one:
            log.warning("species mapping is not 1:1")

        log.info("Translation table:")
        w = max(max(len(x),len(y)) for x,y in strings)

        for ss in sorted(strings):
            if log.isEnabledFor(logging.DEBUG):
                log.debug("{:^{w}s} ===> {:^{w}s}".format(*ss, w=w))
            else:
                if ss[0] != ss[1]:
                    log.info("{:^{w}s} ===> {:^{w}s}".format(*ss, w=w))

        if len(failures)>0:
            for sp,destSp in failures:
                log.error("source species {} transformed to {} does not exist in destination".format(sp,destSp))
            exit(-1)

        sites = hdf['Model']['Diffusion']['LatticeSites']
        siteLattice = numpy.zeros(sites.shape, dtype=sites.dtype)
        sites.read_direct(siteLattice)
        

if args.distribute is not None:
    distList = []
    for st in args.distribute.split(':'):
        vs = st.split(',')
        sp = vs[0]
        ct = int(vs[1])
        compartments = set( int(x) for x in vs[2:] )
        distList.append( (sp, ct, compartments) )



for fIdx in range(args.nEnsemble):
    fname = outputFile(fIdx)
    shutil.copy(args.template,  fname)

    with h5py.File(args.source, "r") as hdf:
            if args.randomReplicate:
                replicate = None

                while replicate is None:
                    rstr = "{:07d}".format(random.randint(1,maxReplicate))
                    if rstr in hdf['Simulations'].keys():
                        replicate = rstr
            else:
                replicate = "{:07d}".format(maxReplicate)

            maxFrame = max( int(f) for f in hdf['Simulations'][replicate]['Lattice'].keys() )

            if args.randomFrame:
                frame = None
                if args.frameStart is not None:
                    minFrame = maxFrame - args.frameStart
                else:
                    minFrame = 0

                while frame is None:
                    fstr = "{:010d}".format(random.randint(minFrame,maxFrame))
                    if fstr in  hdf['Simulations'][replicate]['Lattice'].keys():
                        frame = fstr
            else:
                frame="{:010d}".format(maxFrame)

            log.info("initial species configuration from {}:{}:{}".format(args.source, replicate, frame))

            hdfLattice = hdf['Simulations'][replicate]['Lattice'][frame]
            lattice=numpy.zeros(hdfLattice.shape, dtype=hdfLattice.dtype)
            hdfLattice.read_direct(lattice)
            lx,ly,lz,pps = hdfLattice.shape


    if args.limitCounts:
        spsCts = numpy.bincount(lattice.ravel())
        for spsIdx in (i for i,c in enumerate(spsCts) if c >= args.limitCounts):
            if spsIdx == 0:
                continue

            spsCt=spsCts[spsIdx]
            sp = sourceNames[spsIdx-1]

            log.info("{} is over limit: {}".format(sp, spsCt))

            loc = []
            s = lattice.shape
            for idx,lsp in enumerate(lattice.ravel()):
                if lsp == spsIdx:
                    # idx = p + z*s[3] + y*s[2]*s[3] + x*s[1]*s[2]*s[3]
                    x  = idx//(s[1]*s[2]*s[3])
                    y  = (idx//(s[2]*s[3])) % s[1]
                    z  = (idx//s[3]) % s[2]
                    loc.append((x,y,z))

            removed = 0
            while removed < spsCt-args.limitCounts:
                x,y,z = random.choice(loc)
                site = lattice[x,y,z,:]

                if spsIdx in site:
                    i = list(site).index(spsIdx)
                    site[i:-1] = site[i+1:]
                    site[-1] = 0
                    removed += 1


    newLattice = numpy.vectorize(lambda x: translationTable[x])(lattice)


    

    if args.distribute is not None:
        ridx = lambda x: random.randint(0,x-1)

        for sp, ct, comparments in distList:
            spIdx = destMap[sp]
            log.info("Adding {} {} particles into compartments {}".format(ct,sp,', '.join(map(str,compartments))))
            n = 0
            while n < ct:
                x,y,z = ridx(lx), ridx(ly), ridx(lz)
                if siteLattice[x,y,z] in compartments:
                    for i in range(pps):
                        if newLattice[x,y,z,i] == 0:
                            newLattice[x,y,z,i] = spIdx
                            n += 1
                            break

    with h5py.File(fname) as hdf:
            hdfNewLattice = hdf['Model']['Diffusion']['Lattice']
            hdfNewCounts = hdf['Model']["Reaction"]['InitialSpeciesCounts']

            speciesCounts = numpy.zeros(hdfNewCounts.shape, dtype=hdfNewCounts.dtype)
            for j in newLattice.ravel():
                if j > 0:
                    speciesCounts[j-1] += 1

            hdfNewLattice.write_direct(newLattice)
            hdfNewCounts.write_direct(speciesCounts)

    log.info("wrote {}".format(fname))

