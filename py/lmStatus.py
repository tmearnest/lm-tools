#!/usr/bin/python

import argparse, h5py, numpy, logging, re, os, os.path
import matplotlib.pylab as plt

parser = argparse.ArgumentParser(description="Print info on simulation file")
parser.add_argument('files', nargs='+', metavar='LM_HDF5', type=str, help='HDF5 simulation file')
args = parser.parse_args()

for fname in args.files:
    with  h5py.File(fname, "r") as hdf:
        tf = float(hdf['Parameters'].attrs['maxTime'])
        for r,sim in hdf['Simulations'].items():
            t = sim['LatticeTimes'][-1]
            n = len(sim['Lattice'])
            lbl = fname.split('/')[-1]
            print ("{}:{}: {} frames at {:.2f} sec.; {:.2f}% complete".format(lbl, r, n, t,100*t/tf))

