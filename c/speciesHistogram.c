#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <assert.h>
#include <argp.h>
#include <hdf5.h>
#include <hdf5_hl.h>


const char * argp_program_version = __FILE__ " 0.0.0";
const char *argp_program_bug_address = "Tyler Earnest <earnest3@illinois.edu>";
static char docStr[] = "Extract spatially binned histograms for all species.";
static char argDocStr[] = "histogram.npy file1.lm file2.lm ...";
static struct argp_option options[] = {
  {"verbose", 'v', 0, 0, "Print progress info"},
  {"replicate-range", 'R', "F0:F1:STRIDE", 0, 
    "Replicates to use."},
  {"frame-range", 'F', "F0:F1:STRIDE", 0, 
    "Frames to use."},
  { 0 }
};

struct Slice {
    int i0,i1,di;
    bool start, end, stride;
};

struct Arguments {
  int verbose;
  struct Slice frameSlice, replicateSlice; 
  size_t nLmFiles;
  size_t lmFileListBufSz;
  char **lmFiles;
  char *npyFile;
};



struct Slice
interpretRange(char *str)
{
  struct Slice S;
  S.start = S.end = S.stride = false;
  char *convPtr;
  char *si0 = malloc(strlen(str)+1);
  char *si1=NULL, *sdi=NULL;
  strcpy(si0, str);

  char *c0 = index(si0, ':');
  char *c1 = rindex(si0, ':');

  if (c0 != NULL) {
      *c0 = 0;
      si1 = c0+1;
  }

  if (c1 != NULL && c0 != c1) {
      *c1 = 0;
      sdi = c1+1;
  }

  S.i0 = strtol(si0, &convPtr, 0);
  S.start = true;
  if (convPtr == si0) {
      S.i0 = 0;
      S.start = false;
  }

  if (si1 != NULL && strlen(si1) > 0) {
    S.i1 = strtol(si1, &convPtr, 0);
    S.end = true;
    if (convPtr == si1) {
        S.i1 = 0;
        S.end = false;
    }
  } else {
    S.i1 = 0;
    S.end = false;
  }

  if (sdi != NULL && strlen(sdi) > 0) {
    S.di = strtol(sdi, &convPtr, 0);
    S.stride = true;
    if (convPtr == sdi) {
        S.di = 0;
        S.stride = false;
    }
  } else {
    S.di = 0;
    S.stride = false;
  }

  return S;

}
      

static error_t
parseOpt (int key, char *arg, struct argp_state *state) {
  struct Arguments *args = state->input;
     
  switch (key) {
    case 'R':
      args->replicateSlice = interpretRange(arg);
      break;

    case 'F':
      args->frameSlice = interpretRange(arg);
      break;
      
    case 'v':
      args->verbose++;
      break;
 
    case ARGP_KEY_ARG:
      if (state->arg_num == 0)
        args->npyFile = arg;
      else {
          if (args->lmFileListBufSz == 0) {
              args->lmFileListBufSz = 256;
              args->lmFiles = malloc(sizeof(char *)*args->lmFileListBufSz);
          } else if (args->lmFileListBufSz <= args->nLmFiles) {
              args->lmFileListBufSz *= 2;
              args->lmFiles = realloc(args->lmFiles, sizeof(char *)*args->lmFileListBufSz);
          }

          args->lmFiles[args->nLmFiles] = arg;
          args->nLmFiles ++;
      }
      break;
 
     default:
      return ARGP_ERR_UNKNOWN;
     }
    return 0;
}
     
static struct argp argp = {options, parseOpt, argDocStr, docStr};


#define MAX_STR 256

#define H5EVAL_(stmt) do { hid_t x; H5EVAL(x,stmt); } while(0)

#define H5EVAL(x,stmt) \
  do { \
      x=stmt; \
      if (x < 0) { \
          fprintf(stderr, "%s: hdf call failed at "__FILE__":%d\n", argv[0],__LINE__); \
          exit(-1); \
      } \
  } while(0)


struct Slice
subvaluesSlice(struct Slice S, int minIdx, int maxIdx)
{
  struct Slice S_;

  if (S.start + S.end + S.stride == 0) {
      S_.i0 = minIdx;
      S_.i1 = maxIdx;
      S_.di = 1;
      S_.stride = S_.end = S_.start = true;
  } else if (S.start && !S.end && !S.stride) {
      S_.i0 = S.i0 < minIdx ? minIdx : S.i0;
      S_.i1 = S_.i0+1;
      S_.di = 1;
  } else {
    if (!S.start) {
      S_.i0 = minIdx;
      S_.start = true;
    } else {
      S_.i0 = S.i0;
      S_.start = true;
    }

    if (S_.i0 < 0)
      S_.i0 += maxIdx;

    if (!S.end) {
      S_.i1 = maxIdx;
      S_.end = true;
    } else {
      S_.i1 = S.i1;
      S_.end = true;
    }

    if (S_.i1 < 0)
      S_.i1 += maxIdx;

    if (!S.stride) {
      S_.di = 1;
      S_.stride = true;
    } else {
      S_.di = S.di;
      S_.stride = true;
    }
  }

  assert( (S_.i0 <= S_.i1 && S_.di > 0) || (S_.i0 >= S_.i1 && S_.di < 0) );

  return S_;
}


int
main(int argc, char *argv[])
{
  struct Arguments args;
  memset(&args, 0, sizeof(args));
  argp_parse (&argp, argc, argv, 0, 0, &args);

  uint8_t *frameBuf = NULL;
  uint32_t *locBuf = NULL;
  unsigned int dsSize = 0;
  unsigned int latticeSize = 0;
  unsigned int histSize = 0;
  unsigned int Ni, Nj, Nk, Ns, Np;
  Ni = Nj = Nk = Ns = Np = 0;
  size_t nLattice = 0;

  for (int h5Idx=0; h5Idx < args.nLmFiles; h5Idx++) {
      char repStr[MAX_STR], frameStr[MAX_STR];
      hsize_t nRep=0;

      hid_t fileId, simGroupId;
      H5EVAL(fileId, H5Fopen(args.lmFiles[h5Idx], H5F_ACC_RDWR, H5P_DEFAULT));
      H5EVAL(simGroupId, H5Gopen2(fileId, "/Simulations", H5P_DEFAULT));

      H5EVAL_(H5Gget_num_objs(simGroupId, &nRep));
      unsigned int minR=UINT_MAX;
      unsigned int maxR=0;

      for (int i=0; i < nRep; i++) {
          unsigned r = 0;
          char *chk;
          H5EVAL_(H5Gget_objname_by_idx(simGroupId, i, repStr, MAX_STR));
          r = strtoul(repStr, &chk, 10);
          assert(chk != repStr);

          maxR = r > maxR ? r : maxR;
          minR = r < minR ? r : minR;
      }

      if (minR == UINT_MAX) {
          fprintf(stderr, "%s: No replicates found in %s\n",argv[0], args.lmFiles[h5Idx]);
          continue;
      }

      struct Slice rS = subvaluesSlice(args.replicateSlice, minR, maxR+1);

      for (unsigned int rIdx=rS.i0; rIdx < rS.i1; rIdx+=rS.di) {
          snprintf(repStr, MAX_STR, "%07d/Lattice", rIdx);
          
          hid_t latticeGroupId = H5Gopen2(simGroupId, repStr, H5P_DEFAULT);

          if (latticeGroupId < 0) {
            fprintf(stderr, "%s: Could not open `%s:/Simulations/%s\n", argv[0], args.lmFiles[h5Idx], repStr);
            continue;
          }

          hsize_t nFrames=0;
          
          H5EVAL_( H5Gget_num_objs(latticeGroupId, &nFrames) );

          unsigned int maxF=0;
          unsigned int minF=UINT_MAX;

          for (int i=0; i < nFrames; i++) {
              unsigned f = 0;
              char *chk;
              H5EVAL_(H5Gget_objname_by_idx(latticeGroupId, i, frameStr, MAX_STR));
              f = strtoul(frameStr, &chk, 10);
              assert(chk != frameStr);

              maxF = f > maxF ? f : maxF;
              minF = f < minF ? f : minF;
          }

          struct Slice fS = subvaluesSlice(args.frameSlice, minF, maxF+1);

          for (unsigned int fIdx=fS.i0; fIdx < fS.i1; fIdx+=fS.di) {
              H5G_stat_t h5info;

              snprintf(frameStr, MAX_STR, "%010d", fIdx);

              if (H5Gget_objinfo(latticeGroupId, frameStr, false, &h5info) < 0 || h5info.type != H5G_DATASET) {
                  fprintf(stderr, "%s: Could not open `%s:/Simulations/%s/%s\n", argv[0], args.lmFiles[h5Idx], repStr, frameStr);
                  continue;
              }

              if (H5Gget_objtype_by_idx(latticeGroupId, fIdx ) == H5G_DATASET) {
                  hsize_t dsDims[4];
                  hid_t frameId, dsSpaceId;
                  H5EVAL(frameId, H5Dopen(latticeGroupId, frameStr, H5P_DEFAULT));

                  H5EVAL(dsSpaceId, H5Dget_space(frameId));
                  assert(H5Sget_simple_extent_ndims(dsSpaceId) == 4);
                  H5EVAL_( H5Sget_simple_extent_dims(dsSpaceId, dsDims, NULL) );

                  if (frameBuf == NULL) {
                      Ni = dsDims[0];
                      Nj = dsDims[1];
                      Nk = dsDims[2];
                      Np = dsDims[3];
                      H5EVAL_(H5LTget_attribute_uint(fileId, "/Model/Diffusion", "numberSpecies", &Ns));

                      latticeSize = Ni*Nj*Nk;
                      histSize = latticeSize*Ns;
                      dsSize = latticeSize*Np;

                      frameBuf = malloc(dsSize*sizeof(uint8_t));
                      locBuf = calloc(histSize, sizeof(uint32_t));
                  } else {
                      unsigned int chk;
                      H5EVAL_(H5LTget_attribute_uint(fileId, "/Model/Diffusion", "numberSpecies", &chk));
                      assert( chk == Ns );
                      assert( Ni == dsDims[0] && Nj == dsDims[1] && Nk == dsDims[2] && Np == dsDims[3] );
                  }

                  H5EVAL_(H5Dread( frameId, H5T_NATIVE_UINT8, H5S_ALL, H5S_ALL, H5P_DEFAULT, frameBuf));
                  H5EVAL_(H5Dclose( frameId ));

                  // lattice indexing: p + Np*(z + Nz*(y+Ny*x));

                  for (int l=0; l < dsSize; l++) {
                      unsigned int s = frameBuf[l];
                      if (s > 0)
                        locBuf[l/Np + (s-1)*latticeSize] += 1;
                  }
                  nLattice ++;

                  if (args.verbose > 2)
                    fprintf(stderr, "%s: read %s:%07d:%010d.\n", argv[0], args.lmFiles[h5Idx], rIdx, fIdx);
              }
          }
          H5EVAL_(H5Gclose(latticeGroupId));
          if (args.verbose == 2)
                fprintf(stderr, "%s: read %s:%07d.\n", argv[0], args.lmFiles[h5Idx], rIdx);
      }
      H5EVAL_(H5Gclose(simGroupId));
      H5EVAL_(H5Fclose(fileId));
      if (args.verbose == 1)
          fprintf(stderr, "%s: read %s.\n", argv[0], args.lmFiles[h5Idx]);
  }

  double *normHist = malloc(histSize*sizeof(double));
  double norm = 1.0/nLattice;
  for (int i=0; i < histSize; i++)
    normHist[i] = locBuf[i]*norm;

  FILE *binFp = fopen(args.npyFile, "wb");
  size_t bytes=0;
  bytes += fwrite("\x93NUMPY\x01\x00", 1, 8, binFp);
  char header[MAX_STR];
  memset(header, ' ', MAX_STR);
  int hdrLen = snprintf(header,MAX_STR, "{'descr': 'float64', 'fortran_order': False, 'shape': (%u, %u, %u, %u), }", 
                        Ns, Ni, Nj, Nk);
  header[hdrLen] = ' ';
  uint16_t hdrSize =  16*(1+(hdrLen / 16));
  bytes += fwrite(&hdrSize, 1, sizeof(hdrSize), binFp);
  bytes += fwrite(header, 1, hdrSize, binFp);
  bytes += fwrite(normHist, 1, sizeof(double)*histSize, binFp);
  fclose(binFp);

  if (args.verbose > 0)
      fprintf(stderr, "%s: wrote %zd MB to %s. %zd frames binned.\n", argv[0], bytes>>20, args.npyFile, nLattice);

  free(frameBuf);
  free(locBuf);
  free(normHist);

  return 0;

}
