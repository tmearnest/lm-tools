// g++ -Wall -Werror -Wfatal-errors -std=c++11 -o extractBlenderData extractBlenderData.cc -lhdf5_cpp -ljsoncpp -lhdf5

#include <sstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <set>
#include <map>
#include <regex>
#include <cstdlib>
#include <cmath>
#include <exception>
#include <H5Cpp.h>
#include <json/json.h>

struct Species {
    int x,y,z,t;
    Species(int x, int y, int z, int t) : x(x), y(y), z(z), t(t) {}
};

bool operator < (const Species& l, const Species& r) { return (l.x < r.x) || (l.y < r.y) || (l.z < r.z) || (l.t < r.t); }
bool operator == (const Species& l, const Species& r) { return (l.x == r.x) || (l.y == r.y) || (l.z == r.z) || (l.t == r.t); }


class AssertionError : public std::exception
{
  std::string msg;
public:
  virtual const char* what() const throw()
    {
      return msg.c_str();
    }
  AssertionError(const char* file, int line, const char* test)
    {
      std::ostringstream ss;
      ss << file << ":" << line << ": " << "Assertion `" << test << "' failed";
      msg = ss.str();
    }
};


#define ASSERT(x) do { if (!(x)) throw AssertionError(__FILE__, __LINE__, #x); } while (0)


std::string _program_name_;

[[ noreturn ]]
void
usage()
{
  std::cerr << "usage: " << _program_name_ << " input.json output.h5" << std::endl;
  std::exit(-1);
}


int
main (int argc, char** argv)
{
  std::string name(argv[0]);
  size_t pos = name.rfind('/');

  if ( pos != name.npos)
    _program_name_ = name.substr(pos+1);
  else
    _program_name_ = name;

  if (argc != 3)
    usage();

  try
    {
      int Nx,Ny,Nz,Np;
      Nx=Ny=Nz=0;
      std::ostringstream ss;

      // open input file

      std::ifstream in_file;
      in_file.exceptions(in_file.failbit);
      in_file.open(argv[1]);
      Json::Value json;
      in_file >> json;
      ASSERT(json.isMember("lmFile"));

      // open simulation file
      
      H5::H5File lm(json["lmFile"].asString().c_str(), H5F_ACC_RDONLY);

      // open output file
      
      H5::H5File h5out(argv[2], H5F_ACC_TRUNC);
      H5::Group out_siteGroup(h5out.createGroup("/Sites"));
      H5::Group out_speciesGroup(h5out.createGroup("/Species"));

      // site data: Nx3, /site/siteType
      // species data: Nx3, /frame/speciesClass

      // read species names
      
      H5::Group group(lm.openGroup("/Parameters"));
      H5::Attribute attrib(group.openAttribute("speciesNames"));
      H5::DataType type(attrib.getDataType());

      std::string speciesNames;

      attrib.read(type, speciesNames);
      std::istringstream iss(speciesNames);
      std::map<std::string, int> particleMap;
      std::vector<std::string> particleList;
      particleList.push_back("__empty__");

      int ct=1;
      while (iss)
        {
          char buf[256];
          iss.getline(buf,256,',');
          std::string sp(buf);
          if (sp.size() > 0)
            {
              particleMap[sp] = ct;
              particleList.push_back(sp);
            }
          ct++;
        }

      // read site type data
      
      std::map<std::string, int> siteTypeMap;
      std::map<int, std::string> invSiteTypeMap;
      ASSERT(json.isMember("siteTypes"));
      for (const auto& st: json["siteTypes"])
        {
          ASSERT(st.isMember("name"));
          ASSERT(st.isMember("index"));
          siteTypeMap[st["name"].asString()] = st["index"].asInt();
          invSiteTypeMap[st["index"].asInt()] = st["name"].asString();
        }
      
      // generate species idx mapping
      
      std::vector<int> spsIdxToSpsClass(256);
      for (auto& v: spsIdxToSpsClass) v=-1;
      std::vector<std::string> speciesClassNames;
      ASSERT(json.isMember("speciesTypes"));
      for (const auto& st: json["speciesTypes"])
        {
          ASSERT(st.isMember("name"));
          std::string name(st["name"].asString());
          speciesClassNames.push_back(name);

          ASSERT(st.isMember("regex"));
          std::regex spsMatch(st["regex"].asString());

          for (const auto& labelElem: particleMap)
            if (std::regex_match(labelElem.first, spsMatch))
                spsIdxToSpsClass[labelElem.second] = speciesClassNames.size()-1;
        }

      // read lattice parameters

      group = lm.openGroup("/Model/Diffusion");
      attrib = group.openAttribute("latticeXSize");
      type = attrib.getDataType();
      attrib.read(type, &Nx);

      attrib = group.openAttribute("latticeYSize");
      type = attrib.getDataType();
      attrib.read(type, &Ny);

      attrib = group.openAttribute("latticeZSize");
      type = attrib.getDataType();
      attrib.read(type, &Nz);
      
      attrib = group.openAttribute("particlesPerSite");
      type = attrib.getDataType();
      attrib.read(type, &Np);

      // read site types

      H5::DataSet dataset = lm.openDataSet("/Model/Diffusion/LatticeSites");

      int *lattice = new int[Nx*Ny*Nz];

      dataset.read(lattice, H5::PredType::NATIVE_INT);

      std::map<std::string,std::vector<int>> sites;

      // lattice indexing: z + Nz*(y+Ny*x)
      for (int x=0; x<Nx; x++)
      for (int y=0; y<Ny; y++)
      for (int z=0; z<Nz; z++)
        {
          int type = lattice[z+Nz*(y+Ny*x)];
          auto it = invSiteTypeMap.find(type);
          if (it != invSiteTypeMap.end())
            {
              sites[it->second].push_back(x);
              sites[it->second].push_back(y);
              sites[it->second].push_back(z);
            }
        }

      delete [] lattice;

      // write site types
      for (auto& st: sites)
        {
          const std::string& name = st.first;
          int *data = &(st.second[0]);
          size_t Nsites = st.second.size()/3;
          ss.str("");
          ss << "/Sites/" << name;
          std::string opath = ss.str();
          std::string ipath = "/Model/Diffusion/LatticeSites";

          hsize_t dim[] = {Nsites, 3};
          H5::DataSpace dspace(2, dim);
          H5::DataSet dataset(h5out.createDataSet(opath, H5::PredType::NATIVE_INT, dspace));
          dataset.write(data, H5::PredType::NATIVE_INT);

          std::cout << ipath << " -> " << opath << std::endl;
        }

      // read frame indexing parameters

      ASSERT(json.isMember("replicate"));
      int repId = json["replicate"].asInt();
      ASSERT(json.isMember("frameStart"));
      int frame0 = json["frameStart"].asInt();
      ASSERT(json.isMember("frameStart"));
      int frame1 = json["frameEnd"].asInt();
      ASSERT(json.isMember("frameStep"));
      int dframe = json["frameStep"].asInt();
      
      // read species
      
      lattice = new int[Np*Nx*Ny*Nz];

      int Nframes = (frame1-frame0)/dframe;
      int fieldW = int(std::ceil(std::log10(Nframes)));

      size_t maxSpheres = 0;

      for (int fIdx=frame0; fIdx < frame1; fIdx+=dframe)
        {
          std::vector<std::vector<int>> species(speciesClassNames.size());
          std::set<Species> sSet;

          ss.str("");
          ss << "/Simulations/" << std::setfill('0') << std::setw(7) << repId << "/Lattice/" << std::setw(10) << fIdx;
          std::string ipath = ss.str();

          H5::DataSet dataset = lm.openDataSet(ipath);
          dataset.read(lattice, H5::PredType::NATIVE_INT);

          // lattice indexing: p + Np*(z + Nz*(y+Ny*x));
          for (int x=0; x<Nx; x++)
          for (int y=0; y<Ny; y++)
          for (int z=0; z<Nz; z++)
          for (int p=0; p<Np; p++)
            {
              int t = spsIdxToSpsClass[lattice[p + Np*(z + Nz*(y+Ny*x))]];
              if (t>=0)
                sSet.emplace(x,y,z,t);
            }

          for (const auto& s: sSet)
            {
              species[s.t].push_back(s.x);
              species[s.t].push_back(s.y);
              species[s.t].push_back(s.z);
            }

          maxSpheres = std::max(maxSpheres, sSet.size());

          ss.str("");
          ss << "/Species/" << std::setfill('0') << std::setw(fieldW) << fIdx;
          std::string framePath(ss.str());
          H5::Group out_frameGroup(h5out.createGroup(framePath));

          for (size_t spIdx=0; spIdx < species.size(); spIdx++)
            {
              const std::string& spsName = speciesClassNames[spIdx];
              int *data = &(species[spIdx][0]);
              size_t Nsps = species[spIdx].size()/3;
              ss.str("");
              ss << framePath << "/" << spsName;
              std::string opath = ss.str();
              hsize_t dim[] = {Nsps, 3};
              H5::DataSpace dspace(2, dim);
              H5::DataSet dataset(h5out.createDataSet(ss.str(), H5::PredType::NATIVE_INT, dspace));
              dataset.write(data, H5::PredType::NATIVE_INT);

              std::cout << ipath << " -> " << opath << std::endl;
            }
        }

      H5::IntType intType(H5::PredType::NATIVE_INT);
      H5::DataSpace attSpace(H5S_SCALAR);

      group = h5out.openGroup("/Species");
      attrib = group.createAttribute("maxSpheres", intType, attSpace);
      attrib.write( intType, &maxSpheres );

      delete [] lattice;

      // displacements
     
      ss.str("");
      ss << "/SiteNoise";
      std::string noisePath(ss.str());
      H5::Group out_frameGroup(h5out.createGroup(noisePath));

      std::default_random_engine generator;
      std::uniform_real_distribution<double> distribution(0.0,1.0);

      size_t Nl = Nx*Ny*Nz*3;
      double* fLattice = new double[Nl];

      for (size_t spIdx=0; spIdx < speciesClassNames.size(); spIdx++)
        {
          for (size_t i=0; i < Nl; i++) 
            fLattice[i] = distribution(generator)-0.5;
          
          const std::string& spsName = speciesClassNames[spIdx];
          ss.str("");
          ss << noisePath << "/" << spsName;
          std::string opath = ss.str();
          hsize_t dim[] = {hsize_t(Nx),hsize_t(Ny),hsize_t(Nz),3};
          H5::DataSpace dspace(4, dim);
          H5::DataSet dataset(h5out.createDataSet(ss.str(), H5::PredType::NATIVE_DOUBLE, dspace));
          dataset.write(fLattice, H5::PredType::NATIVE_DOUBLE);

          std::cout << "(rng) -> " << opath << std::endl;
        }
      
      delete [] fLattice;

      // dim
     
      {
        int dims[] = {Nx,Ny,Nz};
        hsize_t dim[] = {3};
        H5::DataSpace dspace(1, dim);
        H5::DataSet dataset(h5out.createDataSet("LatticeSize", H5::PredType::NATIVE_INT, dspace));
        dataset.write(dims, H5::PredType::NATIVE_INT);
        std::cout << "(dims) -> /LatticeSize" << std::endl;
      }
    }
  catch (H5::Exception &e)
    {
      std::cerr << _program_name_ << " failed: HDF5 error" << std::endl
                << "     " << e.getFuncName() << ": " << e.getDetailMsg() << std::endl;
    }
  catch (std::exception &e)
    {
      std::cerr << _program_name_ << " failed: " << e.what() << std::endl;
      std::exit(1);
    }



}


